//
//  AdItem.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import Foundation

struct AdItem: Codable {
    let id: Int
    let categoryId: Int
    let title: String
    let description: String
    let price: Int
    let imagesUrl : ImageUrl
    let creationDate: Date
    let isUrgent: Bool
    let siret: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case categoryId = "category_id"
        case title = "title"
        case description = "description"
        case price = "price"
        case imagesUrl = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
        case siret = "siret"
    }
}
