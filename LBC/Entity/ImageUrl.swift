//
//  ImageUrl.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import Foundation

struct ImageUrl: Codable {
    let small: String?
    let thumb: String?

    enum CodingKeys: String, CodingKey {
        case small = "small"
        case thumb = "thumb"
    }
}
