//
//  Category.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import Foundation

struct Category: Codable {
    
    let id: Int
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
