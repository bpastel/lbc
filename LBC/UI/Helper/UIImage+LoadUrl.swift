//
//  UIImage+LoadUrl.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import UIKit

extension UIImageView {
    func load(url: URL) {
        URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let _ = self,
                  error == nil,
                  let response = response as? HTTPURLResponse, response.statusCode == 200,
                  let data = data,
                  let image = UIImage(data: data)
            else {
                DispatchQueue.main.async {
                    self?.image = UIImage(named: "no-picture")
                    self?.contentMode = .center
                    self?.backgroundColor = .systemGray3
                    self?.layer.cornerRadius = 8
                }
                return
            }
            DispatchQueue.main.async {
                self!.image = image
            }
        }.resume()
    }
    
    convenience init(named name: String, contentMode: UIView.ContentMode) {
        self.init(image: UIImage(named: name))
        self.contentMode = contentMode
    }
    
    convenience init(contentMode: UIView.ContentMode, cornerRadius: CGFloat = 0) {
        self.init()
        self.contentMode = contentMode
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}


