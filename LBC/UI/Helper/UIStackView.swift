//
//  UIStackView.swift
//  LBC-2022
//
//  Created by rose on 29/01/2022.
//

import UIKit

extension UIStackView {
    convenience init(axis: NSLayoutConstraint.Axis, alignment: Alignment? = .fill, distribution: Distribution? = .fill, spacing: CGFloat? = 0, backgroundColor: UIColor? = .systemBackground ) {
        self.init(frame: .zero)
        self.axis = axis
        self.alignment = alignment ?? .fill
        self.distribution = distribution ?? .fill
        self.spacing = spacing ?? 0
        self.backgroundColor = backgroundColor
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}
