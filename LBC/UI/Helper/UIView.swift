//
//  UIView.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import UIKit

extension UIView {
    func addSubviews(_ views: UIView...) {
        views.forEach {view in addSubview(view)}
    }
    
    func setBackgroundAlpha(_ alpha: CGFloat) {
        backgroundColor = backgroundColor?.withAlphaComponent(alpha)
    }
}
