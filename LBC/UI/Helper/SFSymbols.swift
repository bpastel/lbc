//
//  SFSymbols.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import UIKit

struct SFSymbols {
    static let filterLogo = UIImage(systemName: "line.3.horizontal.decrease.circle")
    static let categories =
    [
        UIImage(systemName: "ellipsis"),
        UIImage(systemName: "car"),
        UIImage(systemName: "tshirt"),
        UIImage(systemName: "screwdriver"),
        UIImage(systemName: "house"),
        UIImage(systemName: "sportscourt"),
        UIImage(systemName: "building.2"),
        UIImage(systemName: "books.vertical"),
        UIImage(systemName: "iphone"),
        UIImage(systemName: "hand.thumbsup"),
        UIImage(systemName: "pawprint"),
        UIImage(systemName: "ladybug.fill"),
    ]
    
    /*
     Toutes catégories
     Véhicule
     Mode
     Bricolage
     Maison
     Loisirs
     Immobilier
     Livres CD DVD
     Multimédia
     Service
     Animaux
     Enfants
     */
}
