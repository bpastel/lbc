//
//  DetailsViewController.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var item: AdItem!
    
    private let scrollView: UIScrollView = .init()
    private let contentView: UIView = .init()
    private let imageView: UIImageView = .init(contentMode: .scaleAspectFit, cornerRadius: 10)
    private let urgentBadge: BadgeView = .init()
    private let titleLabel: UILabel = .init()
    private let price: UILabel = .init()
    private let descriptionLabel: UILabel = .init()
    private let date: UILabel = .init()

    init(item: AdItem) {
        super.init(nibName: nil, bundle: nil)
        self.item = item
        urgentBadge.isHidden = !item.isUrgent
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureController()
        configureView()
        updateView()
        constrainView()
    }
    
    private func configureController() {
        view.backgroundColor = .systemBackground
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.isHidden = true
    }
    
    private func configureView() {
        scrollView.scrollsToTop = true
        scrollView.alwaysBounceVertical = true
        
        imageView.backgroundColor = .black
        imageView.layer.cornerRadius = 0
        imageView.isUserInteractionEnabled = true
        
        urgentBadge.label.text = "Urgent"
        
        titleLabel.text = item.title
        titleLabel.numberOfLines = 6
        titleLabel.textAlignment = .left
        titleLabel.font = .systemFont(ofSize: 20, weight: .semibold)
        
        price.font = .systemFont(ofSize: 20, weight: .semibold)
        
        date.font = .systemFont(ofSize: 16, weight: .light)
        
        descriptionLabel.textAlignment = .justified
    }
    
    
    private func updateView() {
        if let urlString = item.imagesUrl.small,
           let url = URL(string: urlString) {
            imageView.load(url: url)
        } else {
            imageView.image = UIImage(named: "no-picture")
            imageView.contentMode = .center
            imageView.backgroundColor = .systemGray3
        }
        
        price.text = "\(item.price)€"
        
        descriptionLabel.text = item.description
        descriptionLabel.numberOfLines = 0

        titleLabel.text = item.title
        
        date.text = item.creationDate.toDateString(format: .dateFR)
    }
    
    
    
    private func constrainView() {
        let paddingVertical: CGFloat = 10
        let paddingLateral: CGFloat = 16

        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubviews(imageView, urgentBadge, titleLabel, price, date, descriptionLabel)
        contentView.directionalLayoutMargins = .init(top: 0, leading: paddingLateral, bottom: paddingVertical, trailing: paddingLateral)

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        urgentBadge.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        price.translatesAutoresizingMaskIntoConstraints = false
        date.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor
                .constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor
                .constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor
                .constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor
                .constraint(equalTo: view.bottomAnchor),
            
            contentView.topAnchor
                .constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor
                .constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor
                .constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor
                .constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor
                .constraint(equalTo: scrollView.widthAnchor),
            contentView.heightAnchor
                .constraint(equalToConstant: UIScreen.main.bounds.height * 1.5),
            
            imageView.topAnchor
                .constraint(equalTo: scrollView.topAnchor),
            imageView.centerXAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.centerXAnchor),
            imageView.widthAnchor
                .constraint(equalTo: scrollView.widthAnchor),
            imageView.heightAnchor
                .constraint(equalTo: imageView.widthAnchor),

            urgentBadge.bottomAnchor
                .constraint(equalTo: imageView.bottomAnchor, constant: -paddingLateral),
            urgentBadge.trailingAnchor
                .constraint(equalTo: contentView.trailingAnchor, constant: -paddingLateral),
            urgentBadge.heightAnchor
                .constraint(equalToConstant: 25),
            urgentBadge.widthAnchor
                .constraint(equalToConstant: 80),
            
            titleLabel.topAnchor
                .constraint(equalTo: imageView.bottomAnchor, constant: paddingVertical),
            titleLabel.leadingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            titleLabel.trailingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            titleLabel.heightAnchor
                .constraint(equalToConstant: 50),
            
            price.topAnchor
                .constraint(equalTo: titleLabel.bottomAnchor, constant: paddingVertical),
            price.leadingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            price.trailingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            price.heightAnchor
                .constraint(equalToConstant: 25),
            
            date.topAnchor
                .constraint(equalTo: price.bottomAnchor, constant: paddingVertical),
            date.leadingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            date.trailingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            date.heightAnchor
                .constraint(equalToConstant: 30),

            descriptionLabel.topAnchor
                .constraint(equalTo: date.bottomAnchor, constant: paddingVertical),
            descriptionLabel.leadingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            descriptionLabel.trailingAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            descriptionLabel.bottomAnchor
                .constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor)
        ])
    }
    
}
