//
//  CategoriesVC.swift
//  LBC-2022
//
//  Created by rose on 27/01/2022.
//

import UIKit

class CategoriesVC: UIViewController {
    
    var categories: [Category]!
    weak var delegateListing: ListingViewController!
    
    private let tableView: UITableView = .init()
     
    convenience init(categories: [Category]?) {
        self.init(nibName: nil, bundle: nil)
        self.categories = categories
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        constrain()
    }
    
    private func configure() {
        view.backgroundColor = .systemBackground
        
        let buttonDone: UIBarButtonItem = .init(barButtonSystemItem: .done, target: self, action: #selector(dismissViewController))
        buttonDone.tintColor = .systemOrange
        navigationItem.rightBarButtonItem = buttonDone
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CategoryCell.self, forCellReuseIdentifier: CategoryCell.reuseIdentifier)
        tableView.rowHeight = 50
        tableView.separatorStyle = .singleLine
    }
    
    private func constrain() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor
                .constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor
                .constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor
                .constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor
                .constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    @objc func dismissViewController() {
        dismiss(animated: true)
    }
}

extension CategoriesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCell.reuseIdentifier, for: indexPath) as! CategoryCell
        let title = indexPath.row == 0 ? "Toutes catégories" : categories[indexPath.row-1].name
        cell.titleLabel.text = title
        cell.icon.image = SFSymbols.categories[indexPath.row] ?? UIImage(systemName: "ellipsis")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexPath.row == 0 ? delegateListing.resetSelector() : delegateListing.didSelectCategory(categories[indexPath.row-1])
        dismissViewController()
    }
}
