//
//  CategoryCell.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    static let reuseIdentifier = "CategoryCellID"
    
    var icon: UIImageView = .init()
    let titleLabel: UILabel = .init()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func configure() {
        icon.tintColor = .systemGray3
        icon.contentMode = .scaleAspectFit
        titleLabel.font = .systemFont(ofSize: 16, weight: .regular)
        titleLabel.textColor = .label
    }

    private func constrain() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(icon)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        icon.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            icon.centerYAnchor
                .constraint(equalTo: centerYAnchor),
            icon.leadingAnchor
                .constraint(equalTo: leadingAnchor, constant: 20),
            icon.heightAnchor
                .constraint(equalTo: heightAnchor, multiplier: 0.6),
            icon.widthAnchor
                .constraint(equalTo: icon.heightAnchor),
            
            titleLabel.centerYAnchor
                .constraint(equalTo: icon.centerYAnchor),
            titleLabel.leadingAnchor
                .constraint(equalTo: icon.trailingAnchor, constant: 20),
            titleLabel.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            titleLabel.heightAnchor
                .constraint(equalTo: heightAnchor)
        ])
    }
}
