//
//  AdCollectionCell.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import UIKit

class ListingCell: UICollectionViewCell {
    
    static let reuseIdentifier = "ListingCellId"
    
    let image: UIImageView = .init(contentMode: .scaleAspectFill, cornerRadius: 8)
    let itemCategory: UILabel = .init()
    let itemPrice: UILabel = .init()
    let itemTitle: UILabel = .init()

    private let badgeUrgent: BadgeView = .init()
    private let vStack: UIStackView = .init(axis: .vertical, alignment: .fill, distribution: .fill, spacing: 5)
    private let hStack: UIStackView = .init(axis: .horizontal)
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with adItem: AdItem, category: String) {
        if let urlSmallImage = adItem.imagesUrl.small {
            image.load(url: URL(string: urlSmallImage)!)
            image.contentMode = .scaleAspectFill
        } else {
            image.image = UIImage(named: "no-picture")
            image.contentMode = .center

        }
    
        itemTitle.text = adItem.title
        badgeUrgent.isHidden = !adItem.isUrgent 
        itemPrice.text = "\(adItem.price)€"
        itemCategory.text = category
    }
    
    private func configure() {
        image.backgroundColor = .systemGray3
        itemCategory.font = .systemFont(ofSize: 15, weight: .thin)
        itemTitle.font = .systemFont(ofSize: 16, weight: .semibold)
        itemTitle.numberOfLines = 6
        itemPrice.font = .systemFont(ofSize: 14, weight: .regular)
        badgeUrgent.label.text = "Urgent"
    }
    
    private func constrain() {
        let padding: CGFloat = 12
        contentView.addSubview(image)
        contentView.addSubview(vStack)
        contentView.directionalLayoutMargins = .init(top: 0, leading: padding, bottom: 0, trailing: padding)
        vStack.addArrangedSubview(itemTitle)
        vStack.addArrangedSubview(itemCategory)
        vStack.addArrangedSubview(itemPrice)
        vStack.addArrangedSubview(UIView())
        vStack.addArrangedSubview(hStack)
        hStack.addArrangedSubview(UIView())
        hStack.addArrangedSubview(badgeUrgent)
        
        vStack.translatesAutoresizingMaskIntoConstraints = false
        image.translatesAutoresizingMaskIntoConstraints = false
        itemCategory.translatesAutoresizingMaskIntoConstraints = false
        itemTitle.translatesAutoresizingMaskIntoConstraints = false
        itemPrice.translatesAutoresizingMaskIntoConstraints = false
        badgeUrgent.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            image.centerYAnchor
                .constraint(equalTo: layoutMarginsGuide.centerYAnchor),
            image.leadingAnchor
                .constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            image.heightAnchor
                .constraint(equalTo: layoutMarginsGuide.heightAnchor),
            image.widthAnchor
                .constraint(equalTo: layoutMarginsGuide.widthAnchor, multiplier: 0.5),
            
            vStack.topAnchor
                .constraint(equalTo: image.topAnchor),
            vStack.leadingAnchor
                .constraint(equalTo: image.trailingAnchor, constant: padding),
            vStack.trailingAnchor
                .constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -padding),
            vStack.bottomAnchor
                .constraint(equalTo: image.bottomAnchor),
            
            badgeUrgent.heightAnchor
                .constraint(equalToConstant: 25),
            badgeUrgent.widthAnchor
                .constraint(equalToConstant: 80)
        ])
    }
}

