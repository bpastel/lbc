//
//  ListingLayout.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import UIKit

class ListingLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        headerReferenceSize = .init(width: UIScreen.main.bounds.width, height: 12)
        sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        scrollDirection = .vertical
        minimumInteritemSpacing = 0
        minimumLineSpacing = 12
        let itemWidth = UIScreen.main.bounds.width - sectionInset.left - sectionInset.right
        itemSize = CGSize(width: itemWidth, height: UIScreen.main.bounds.height * 1 / 2.5 )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

