//
//  ListingHeaderView.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import UIKit

protocol HeaderViewDelegate: AnyObject {
    func updateStatusBarStyle(to style: UIStatusBarStyle)
}

class ListingHeaderView: UIView {
    
    weak var delegate: HeaderViewDelegate?
    private var statusBarStyle: UIStatusBarStyle = .lightContent { didSet {delegate?.updateStatusBarStyle(to: statusBarStyle)}}
        
    private let card = UIView()
    private let logo: UIImageView = .init(named: "lbc-logo", contentMode: .scaleAspectFit)
    private var button: UIButton = .init()
    private let separator = UIView()
    
    private let padding: CGFloat = 12
    private let logoHeight: CGFloat = 30
    private let buttonHeight: CGFloat = 36
    private let initialTopSpace: CGFloat = 20
    
    private lazy var maxTopSpace: CGFloat = initialTopSpace
    private lazy var headerMinHeight : CGFloat = { safeAreaInsets.top + buttonHeight + padding * 2 }()
    private lazy var headerMaxHeight: CGFloat = initialTopSpace + logoHeight + buttonHeight + padding * 6
    
    private var constraintHeaderHeightToMax = NSLayoutConstraint()
    private var constraintCardTopToHeaderTop = NSLayoutConstraint()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func safeAreaInsetsDidChange() {
        maxTopSpace = initialTopSpace + safeAreaInsets.top
        constraintCardTopToHeaderTop.constant = maxTopSpace
    }
    
    func configure() {
        backgroundColor = .systemBackground
        card.backgroundColor = .systemBackground
        separator.backgroundColor = .quaternaryLabel

        configureButton()
    }
    
    func constrain() {
        addSubview(card)
        addSubview(logo)
        addSubview(button)
        addSubview(separator)
        
        translatesAutoresizingMaskIntoConstraints = false
        card.translatesAutoresizingMaskIntoConstraints = false
        logo.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        constraintHeaderHeightToMax = heightAnchor
            .constraint(equalToConstant: headerMaxHeight)
        constraintCardTopToHeaderTop = card.topAnchor
            .constraint(equalTo: topAnchor, constant: maxTopSpace)
        
        NSLayoutConstraint.activate([
            constraintHeaderHeightToMax,
            constraintCardTopToHeaderTop,
            
            card.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            card.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            card.bottomAnchor
                .constraint(equalTo: bottomAnchor),
            
            logo.heightAnchor
                .constraint(equalToConstant: logoHeight),
            logo.topAnchor
                .constraint(greaterThanOrEqualTo: card.topAnchor, constant: padding * 2),
            logo.topAnchor
                .constraint(greaterThanOrEqualTo: safeAreaLayoutGuide.topAnchor, constant: padding),
            logo.centerXAnchor
                .constraint(equalTo: card.centerXAnchor),

            button.topAnchor
                .constraint(greaterThanOrEqualTo: card.topAnchor, constant: padding),
            button.centerXAnchor
                .constraint(equalTo: card.centerXAnchor),
            button.bottomAnchor
                .constraint(equalTo: card.bottomAnchor, constant: -padding),
            button.heightAnchor
                .constraint(equalToConstant: buttonHeight),
            
            separator.heightAnchor
                .constraint(equalToConstant: 1),
            separator.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            separator.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            separator.bottomAnchor
                .constraint(equalTo: bottomAnchor)
        ])
    }
    
    func configureButton() {
        var buttonConfig = UIButton.Configuration.filled()
        var attributeContainerForButton = AttributeContainer()
        let font = UIFont.systemFont(ofSize: 16, weight: .regular)
        attributeContainerForButton.font = font
        buttonConfig.attributedTitle = AttributedString("Catégories", attributes: attributeContainerForButton)
        buttonConfig.baseBackgroundColor = .systemBackground
        buttonConfig.baseForegroundColor = .label
        buttonConfig.cornerStyle = .large
        buttonConfig.image = UIImage(systemName: "chevron.right")
        buttonConfig.imagePlacement = .trailing
        buttonConfig.imagePadding = 4
        buttonConfig.contentInsets = .init(top: 30, leading: 30, bottom: 30, trailing: 30)
        buttonConfig.preferredSymbolConfigurationForImage = UIImage.SymbolConfiguration(pointSize: 10)
        button = UIButton(configuration: buttonConfig, primaryAction: nil)
        button.addTarget(nil, action: #selector(ListingViewController.chooseCategory), for: .touchUpInside)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.quaternaryLabel.cgColor
    }
}


extension ListingHeaderView {
    
     var separationY: CGFloat {
        get { constraintHeaderHeightToMax.constant }
        set { animate(to: newValue) }
    }
    
    func updateHeader(newY: CGFloat, oldY: CGFloat) -> CGFloat {
        let delta = newY - oldY
        let isMovingUp = delta > 0
        let isInContent = newY > 0
        let hasRoomToCollapse = separationY > headerMinHeight
        let shouldCollapse = isMovingUp && isInContent && hasRoomToCollapse
        
        let isMovingDown = delta < 0
        let isBeyondContent = newY < 0
        let hasRoomToExpand = separationY < headerMaxHeight
        let shouldExpand = isMovingDown && isBeyondContent && hasRoomToExpand
        
        if shouldCollapse || shouldExpand {
            separationY -= delta
            return newY - delta
        }
        return newY
    }
    
    private func animate(to value: CGFloat) {
        let clampedHeight = max(min(value, headerMaxHeight), headerMinHeight)
        constraintHeaderHeightToMax.constant = clampedHeight
        let normalized = (value - headerMinHeight) / (headerMaxHeight - headerMinHeight)
        logo.alpha = normalized
        switch normalized {
        case ..<0.5:
            animateToFifty(normalized)
        default:
            animateToOneHundred(normalized)
        }
    }
    
    private func animateToFifty(_ normalized: CGFloat) {
        let newTop = normalized * 2 * maxTopSpace
        constraintCardTopToHeaderTop.constant = newTop
    }
    
    private func animateToOneHundred(_ normalized: CGFloat) {
        constraintCardTopToHeaderTop.constant = maxTopSpace
    }
}


