//
//  ListingViewController.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import UIKit

final class ListingViewController: UIViewController {
    
    enum Section { case main }

    private let headerView: ListingHeaderView = .init()
    private lazy var collectionView: UICollectionView = .init(frame: view.bounds, collectionViewLayout: ListingLayout())

    private let fetcherAdItem = Fetcher<AdItem>()
    private let fetcherCategory = Fetcher<Category>()
    private let classifier = Classifier()
    
    private var categories: [Category] = []
    private var itemArray: [AdItem] = [] { didSet { reloadCollectionView() }}
    private var itemArrayFiltered: [AdItem] = [] { didSet { reloadCollectionView() }}
    private var filterCategory: Category?
    
    private var oldYOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureController()
        constrainView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        fetchData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false

        super.viewWillDisappear(animated)
    }
    
    private func configureController() {
        navigationController?.navigationBar.isHidden = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .systemBackground
        collectionView.register(ListingCell.self, forCellWithReuseIdentifier: ListingCell.reuseIdentifier)
    }
    
    private func constrainView() {
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = .systemOrange

        view.addSubview(headerView)
        view.addSubview(collectionView)
                
        headerView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
   
        NSLayoutConstraint.activate([
            headerView.topAnchor
                .constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            headerView.leadingAnchor
                .constraint(equalTo: view.leadingAnchor),
            headerView.trailingAnchor
                .constraint(equalTo: view.trailingAnchor),
            headerView.bottomAnchor
                .constraint(equalTo: collectionView.topAnchor),
            
            collectionView.leadingAnchor
                .constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor
                .constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor
                .constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func fetchData() {
        fetcherCategory.fetch(url: ApiEndpoint.categories) { fetchedResult in
            self.categories = fetchedResult
        }
        fetcherAdItem.fetch(url: ApiEndpoint.listing) {  fetchedResult in
            let sortedByDate = self.classifier.sortItemsByDate(items: fetchedResult)
            let sortedByUrgent = self.classifier.sortItemsByStatus(items: sortedByDate)
            self.itemArray = sortedByUrgent }
    }
    
    private func reloadCollectionView() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    @objc public func chooseCategory() {
        let categoriesVC = CategoriesVC(categories: categories)
        categoriesVC.delegateListing = self
        let navigationController = UINavigationController(rootViewController: categoriesVC)
        present(navigationController, animated: true)
    }
    
    func launchFilter(with category: Category) {
        filterCategory = category
        guard let currentFilter = filterCategory else { return }
        itemArrayFiltered = classifier.filterItemsByCategory(items: itemArray, category: currentFilter)
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
    }
    
    func removeFilter() {
        filterCategory = nil
        itemArrayFiltered.removeAll()
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
    }
}

extension ListingViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if filterCategory != nil { return itemArrayFiltered.count }
        else { return itemArray.count}
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ListingCell.reuseIdentifier, for: indexPath) as! ListingCell
        let adItem: AdItem = filterCategory != nil ? itemArrayFiltered[indexPath.row] : itemArray[indexPath.row]
        if categories.count > 0 {
            let category = categories.filter { $0.id == adItem.categoryId }.map { $0 }[0].name
            cell.update(with: adItem, category: category)
        }
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeader(with: scrollView)
    }
    
    func updateHeader(with scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        let updatedY = headerView.updateHeader(newY: yOffset, oldY: oldYOffset)
        scrollView.contentOffset.y = updatedY
        oldYOffset = scrollView.contentOffset.y
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item: AdItem = filterCategory != nil ? itemArrayFiltered[indexPath.item] : itemArray[indexPath.item]
        let detailsViewController = DetailsViewController(item: item)
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}


extension ListingViewController: CategorySelector {
    func resetSelector() {
        removeFilter()
    }

    func didSelectCategory(_ category: Category) {
        launchFilter(with: category)
    }
}
