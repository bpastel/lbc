//
//  CategorySelector.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import Foundation

protocol CategorySelector: AnyObject {
    func resetSelector()
    func didSelectCategory(_ category: Category)
}
