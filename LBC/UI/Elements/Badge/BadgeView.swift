//
//  BadgeView.swift
//  LBC-2022
//
//  Created by rose on 30/01/2022.
//

import UIKit

class BadgeView: UIView {
    
    let label: UILabel = .init()
    private let backgroundView: UIView = .init()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        backgroundView.layer.cornerRadius = 13
        backgroundView.backgroundColor = .systemOrange.withAlphaComponent(0.85)
        backgroundView.layer.borderWidth = 1
        backgroundView.layer.borderColor = UIColor.systemOrange.cgColor
        label.textColor = .white
        label.font = .systemFont(ofSize: 14, weight: .semibold)
    }
    
    private func constrain() {
        addSubview(backgroundView)
        backgroundView.addSubview(label)
        
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backgroundView.topAnchor
                .constraint(equalTo: topAnchor),
            backgroundView.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            backgroundView.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            backgroundView.bottomAnchor
                .constraint(equalTo: bottomAnchor),
            
            label.heightAnchor
                .constraint(equalTo: backgroundView.heightAnchor),
            label.widthAnchor
                .constraint(equalTo: backgroundView.widthAnchor),
            label.centerYAnchor
                .constraint(equalTo: backgroundView.centerYAnchor),
            label.centerXAnchor
                .constraint(equalTo: backgroundView.centerXAnchor, constant: 18)
            
        ])
    }
}
