//
//  Decoder.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import Foundation

class DataDecoder<T: Decodable> {
    func decodeData(data: Data, completedWith: (_ result: [T]) -> Void) {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        do {
            let decodedData = try decoder.decode([T].self, from: data)
            completedWith(decodedData)
        } catch {
            print(error.localizedDescription)
        }
    }
}
