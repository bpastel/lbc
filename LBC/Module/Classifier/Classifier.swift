//
//  Classifier.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import Foundation

class Classifier {
    func sortItemsByDate(items: [AdItem]) -> [AdItem] {
        items.sorted { ad1, ad2 in ad1.creationDate.compare(ad2.creationDate) == .orderedDescending }
    }
    
    func sortItemsByStatus(items: [AdItem]) -> [AdItem] {
        items.sorted { $0.isUrgent && !$1.isUrgent }
    }
    
    func filterItemsByCategory(items: [AdItem], category: Category) -> [AdItem] {
        items.filter { $0.categoryId == category.id }.map { $0 }
    }
}
