//
//  Fetcher.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import UIKit

class Fetcher<T: Decodable> {
    func fetch(url: String, completion: @escaping ([T]) -> Void) {
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error -> Void in
            guard let data = data else {
                print(error!)
                completion([])
                return
            }
            
            let decoder = DataDecoder<T>()
            
            decoder.decodeData(data: data) { decodedData in
                completion(decodedData)
            }
        }.resume()
    }
}

