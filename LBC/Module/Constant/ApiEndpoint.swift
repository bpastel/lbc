//
//  ApiEndpoint.swift
//  LBC-2022
//
//  Created by rose on 24/01/2022.
//

import Foundation

struct ApiEndpoint {
    static let listing = "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json"
    static let categories = "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json"
}
